.PHONY: all clean

.PHONY: test
test: rubocop

.PHONY: rubocop
rubocop:
	@bundle exec rubocop --parallel

setup:
	@gem update --system
	@bundle install
	@yarn install --frozen-lockfile

# Content audit scripts/helpers
update-all-projects:
	(cd ../gitlab-docs && make update-all-projects)

check-pages-not-in-nav:
	@node ./scripts/content_audit_toolkit/pages_not_in_nav.js

check-page-nav-titles:
	@node ./scripts/content_audit_toolkit/navigation_titles.js
