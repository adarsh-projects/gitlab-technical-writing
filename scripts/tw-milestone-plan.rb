#!/usr/bin/env ruby
# frozen_string_literal: true

# Automatically create an issue based on the TW milestone planning template.
# Make sure to add the GITLAB_API_PRIVATE_TOKEN in the project's CI/CD variables.
#

require 'json'
require 'net/http'
require 'date'
require 'gitlab'

RELEASE_DATES_URI = URI('https://gitlab.com/gitlab-org/gitlab-docs/-/raw/main/content/release_dates.json')

def get_release_dates
  JSON.parse(Net::HTTP.get(RELEASE_DATES_URI)).first
rescue StandardError => e
  raise "An error has occurred - #{e}"
end

def upcoming_milestone
  @upcoming_milestone ||= begin
    # Based on the time the script runs, return the next month.
    upcoming_release_date = (Date.today >> 1).strftime("%Y-%m-22")

    # Search in the relase dates hash for the upcoming release date
    # and fetch the milestone title.
    get_release_dates[upcoming_release_date]
  end
end

template = File.read('.gitlab/issue_templates/tw-milestone-plan.md')
assign_milestone = "/milestone %\"#{upcoming_milestone}\""
description = template + assign_milestone
issue_title = "Technical Writing milestone plan for #{upcoming_milestone}"

# Take the project ID from the CI_PROJECT_ID predefined variable
project_id = ENV.fetch('CI_PROJECT_ID', nil)

gl = Gitlab.client(endpoint: 'https://gitlab.com/api/v4', private_token: ENV.fetch('GITLAB_API_PRIVATE_TOKEN', nil))

gl.create_issue(project_id, issue_title, { description: description })
