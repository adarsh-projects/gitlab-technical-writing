const fs = require("fs");
const yaml = require("js-yaml");

const CONSTANTS = {
  DOCS_BASE_URL: "https://docs.gitlab.com",
  DOCS_DIR: "../gitlab-docs",
};

/**
 * Get data sources consumed by Nanoc.
 *
 * @returns Array
 */
const nanocDataSources = () => {
  const nanocConfig = yaml.load(
    fs.readFileSync(`${CONSTANTS.DOCS_DIR}/nanoc.yaml`, "utf8")
  );
  return nanocConfig.data_sources.filter((source) => source.items_root !== "/");
};

/**
 * Retrieve the global docs site navigation.
 *
 * @returns Object
 */
const getNav = () => {
  return yaml.load(
    fs.readFileSync(
      `${CONSTANTS.DOCS_DIR}/content/_data/navigation.yaml`,
      "utf8"
    )
  );
};

/**
 * Find the title from a given markdown file.
 *
 * @returns String
 */
const getTitleFromMarkdownPage = (fileContent) => {
  const lines = fileContent.split(/\r?\n/);
  let title = "";
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i].trim();
    if (line.startsWith("# ")) {
      title = line.substring(2);
      break;
    }
  }
  return title;
};

const getHTMLpathFromMdPath = (source, filename) => {
  return (
    source.items_root.replaceAll("/", "") +
    filename
      .replace(source.content_dir, "")
      .replace(source, "")
      .replace("index.md", "")
      .replace(".md", ".html")
  );
};

exports.CONSTANTS = CONSTANTS;
exports.nanocDataSources = nanocDataSources;
exports.getNav = getNav;
exports.getTitleFromMarkdownPage = getTitleFromMarkdownPage;
exports.getHTMLpathFromMdPath = getHTMLpathFromMdPath;
