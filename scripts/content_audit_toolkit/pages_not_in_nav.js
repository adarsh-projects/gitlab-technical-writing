#!/usr/bin/env node

/**
 * @file pages_not_in_nav.js
 * Generates a report of pages which are not included in navigation.yaml.
 */

const fs = require("fs");
const glob = require("glob");
const fm = require("front-matter");
const helpers = require("./helpers.js");

// Read the markdown file and extract the fields we need.
const getPageData = (filename) => {
  const contents = fs.readFileSync(filename, "utf-8");
  const title = helpers.getTitleFromMarkdownPage(contents).toLowerCase();

  return {
    filename,
    isRedirect: contents.includes("redirect_to"),
    isDeprecated: title.includes("(deprecated)") || title.includes("(removed)"),
    stage: fm(contents).attributes.stage,
    group: fm(contents).attributes.group,
  };
};

// Loop through each data source's markdown files.
const lostPages = [];
const nav = JSON.stringify(helpers.getNav());
helpers.nanocDataSources().forEach((source) => {
  glob.sync(`${source.content_dir}/**/*.md`).forEach((filename) => {
    const pageData = getPageData(filename);
    if (pageData.isRedirect || pageData.isDeprecated) {
      return;
    }

    // Convert the markdown filepath into a string that matches the URL path on the website.
    const path = helpers.getHTMLpathFromMdPath(source, filename);
    if (
      // Include pages that are not in the nav.
      !nav.includes(path) &&
      // Exclude sections that are intentionally not in the nav.
      !path.includes("/architecture/blueprints") &&
      !path.includes("/user/application_security/dast/checks/") &&
      !path.includes("/legal/") &&
      !path.includes("/drawers/") &&
      !path.includes("/adr/")
    ) {
      lostPages.push({
        url: `https://docs.gitlab.com/${path}`,
        stage: pageData.stage,
        group: pageData.group,
      });
    }
  });
});

// Return results as JSON.
console.log(JSON.stringify(lostPages));
