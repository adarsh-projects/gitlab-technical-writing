# Docs Content Audit Toolkit

Scripts to analyze GitLab docs content.

## Usage

1. Install and update project dependencies: `make setup`
2. Update your local Docs content repositories: `make update-all-projects`
3. See below for instructions to run each script.

## Scripts

### Find pages that are not included in the global navigation

Run this command to check for pages that are missing from the global navigation:

```shell
make check-pages-not-in-nav
```

This [script](../pages_not_in_nav.js) outputs JSON containing matching page URL paths and the associated Section and Group to the console.

Before running the script, you may want to run `make update-all-docs-projects` to pull down the latest content from each source project.

You can use `jq` on the command line to extract specific fields. For example, to return a list of only URLs:

```shell
make check-pages-not-in-nav | jq '.[] | .url'
```

The script intentionally omits:

- Sections referenced in the ["Pages you don’t need to add"](https://docs.gitlab.com/ee/development/documentation/site_architecture/global_nav.html#pages-you-dont-need-to-add) section of our docs.
- Redirects.
- Markdown files that are not compiled to HTML pages (see the `ignore` paths in Nanoc's [Rules](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/Rules) file).

### Compare page and navigation titles

Run the script, which creates a CSV file:

```shell
make check-page-nav-titles
```

Upload the CSV into Google Sheets:

1. Create a new sheet: [https://sheets.new](https://sheets.new).
2. Upload the CSV: File > Import > Upload
3. Default upload options should work fine.
