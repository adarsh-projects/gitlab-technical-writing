#!/usr/bin/env node

/**
 * @file page_nav_titles.js
 * Generates a report of page titles with their navigation titles.
 */

const fs = require("fs");
const glob = require("glob");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const helpers = require("./helpers.js");

// Load the global navigation.
const navData = helpers.getNav();

/**
 * Find the navigation title for a Docs page.
 *
 * We need to use a series of functions here to dig
 * through the three different tiers in navigation.yaml:
 * sections, categories, and docs.
 */
const findTitleInNavSubsection = (docSubsection, url) => {
  if (docSubsection.doc_url === url) {
    return docSubsection.doc_title;
  }
  // Docs subsections can be nested, so searching within these
  // needs to use a recursive function that can handle an arbitrary depth.
  const subDocs = docSubsection.docs || [];
  for (const subDoc of subDocs) {
    const title = findTitleInNavSubsection(subDoc, url);
    if (title) {
      return title;
    }
  }
  return null;
};
const findTitleInNav = (section, url) => {
  if (section.section_url === url) {
    return section.section_title;
  }
  const categories = section.section_categories || [];
  for (const category of categories) {
    if (category.category_url === url) {
      return category.category_title;
    }
    const docs = category.docs || [];
    for (const doc of docs) {
      if (doc.doc_url === url) {
        return doc.doc_title;
      }
      const subDocs = doc.docs || [];
      for (const subDoc of subDocs) {
        const title = findTitleInNavSubsection(subDoc, url);
        if (title) {
          return title;
        }
      }
    }
  }
  return null;
};

const getNavTitleFromHTMLpath = (url) => {
  for (const section of navData.sections || []) {
    const title = findTitleInNav(section, url);
    if (title) {
      return title;
    }
  }
  return "";
};

/**
 * Removes tier labels (e.g, **(FREE)**) from a string.
 */
const dropPricingSuffix = (title) => {
  return title.replace(/\*\*.*$/, "").trim();
};

/**
 * Build report and write to CSV.
 */
const pages = [];
helpers.nanocDataSources().forEach((source) => {
  glob.sync(`${source.content_dir}/**/*.md`).forEach((filename) => {
    const pageContents = fs.readFileSync(filename, "utf-8");

    const mdPagePath = filename.substring(3); // Drop the leading "../"
    const pageTitle = dropPricingSuffix(
      helpers.getTitleFromMarkdownPage(pageContents)
    );
    const htmlPath = helpers.getHTMLpathFromMdPath(source, filename);
    const navTitle = getNavTitleFromHTMLpath(htmlPath, navData);
    const url = `${helpers.CONSTANTS.DOCS_BASE_URL}/${htmlPath}`;

    pages.push({ mdPagePath, pageTitle, navTitle, url });
  });
});

const path = "navigation.csv";
const csvWriter = createCsvWriter({
  path,
  header: [
    { id: "mdPagePath", title: "Markdown path" },
    { id: "navTitle", title: "Navigation title" },
    { id: "pageTitle", title: "Page title" },
    { id: "url", title: "Link" },
  ],
});
csvWriter
  .writeRecords(pages)
  .then(() => console.log(`CSV file written successfully: ${path}`));
